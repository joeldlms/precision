////////////////////////////////////////////
//  Copyright Myrnix.com 2016
//  All Rights Reserved
/////////////////////////////

$(document).ready(function(){

  var pannelHeight = $("#s1").height();
  var scrollTop = $(document).scrollTop();

  console.log('docs ready!');

  if  (scrollTop < pannelHeight) {
    $("header").addClass("hide").removeClass("show");
    //$(".toggl2").addClass("hide").removeClass("show");
    $(".nav").addClass("hide").removeClass("show");
  } else {
    $("header").removeClass("show-header").addClass("hide-header");}


});

$("div.teamBio").addClass("hide");

$('div.figure').hover(function(){
  $(this).find("div.teamBio").removeClass("hide").addClass("show");
}, /*else*/function(){
  $(this).find("div.teamBio").removeClass("show").addClass("hide");

});


$(document).on( 'scroll', function(){
  var pannelHeight = $("#s1").height();
  var scrollTop = $(document).scrollTop();

    console.log('test');
      if  (scrollTop > pannelHeight -50) {
        $("header").addClass("show").removeClass("hide");
        //$(".toggl2").addClass("show").removeClass("hide");
        $(".nav").addClass("show").removeClass("hide");
      } else {
    $(".nav").addClass("hide").removeClass("show");}
});


///// Next/Prev Segment Scrolls...

$(function(){

    var pagePositon = 0,
        sectionsSeclector = 'section',
        $scrollItems = $(sectionsSeclector),
        offsetTolorence = 30,
        pageMaxPosition = $scrollItems.length - 1;

    //Map the sections:
    $scrollItems.each(function(index,ele) { $(ele).attr("debog",index).data("pos",index); });

    // Bind to scroll
    $(window).bind('scroll',upPos);

////
    $('#arrow2 a').click(function(f){
      if ($(this).hasClass('next-arrow') && pagePositon+1 <= pageMaxPosition) {
          pagePositon++;
          $('html, body').stop().animate({
                scrollTop: $scrollItems.eq(pagePositon).offset().top
          }, 300);
      }
      if ($(this).hasClass('previous') && pagePositon-1 >= 0) {
          pagePositon--;
          $('html, body').stop().animate({
                scrollTop: $scrollItems.eq(pagePositon).offset().top
            }, 300);
          return false;
      }
      });


///

    //Move on click:
    $('#arrow a').click(function(e){
      if ($(this).hasClass('next') && pagePositon+1 <= pageMaxPosition) {
          pagePositon++;
          $('html, body').stop().animate({
                scrollTop: $scrollItems.eq(pagePositon).offset().top
          }, 300);
      }
      if ($(this).hasClass('previous') && pagePositon-1 >= 0) {
          pagePositon--;
          $('html, body').stop().animate({
                scrollTop: $scrollItems.eq(pagePositon).offset().top
            }, 300);
          return false;
      }
    });

    //Update position func:
    function upPos(){
       var fromTop = $(this).scrollTop();
       var $cur = null;
        $scrollItems.each(function(index,ele){
            if ($(ele).offset().top < fromTop + offsetTolorence) $cur = $(ele);
        });
       if ($cur != null && pagePositon != $cur.data('pos')) {
           pagePositon = $cur.data('pos');
       }
    }

});
