/// <reference path="bower_components/angular-multi-step-form/dist/browser/angular-multi-step-form.js" />
/// <reference path="bower_components/angular-multi-step-form/dist/browser/angular-multi-step-form.js" />
/// <binding />
/// <reference path="bower_components/Chart.js/Chart.js" />
/// <reference path="bower_components/Chart.js/Chart.js" />
/// <reference path="bower_components/Chart.js/Chart.js" />
var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var ngAnnotate = require('gulp-ng-annotate');
var less = require('gulp-less');
var livereload = require('gulp-livereload');
var bourbon = require("bourbon");


// Source variables

var src = {
    sass: ['scss/Landing.scss'],
    js: ['js/*.js'],
    images: ['img/**/*']
};

gulp.task('default', ['appDependencies', 'ng_annotate', 'scripts', 'landingScripts', 'sass', 'images', 'watch']);

/*** Sass */
gulp.task('sass', function(done) {
    gulp.src(['./scss/Landing.scss'])
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(gulp.dest('./build/css'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./build/css'))
    .on('end', done);
});

gulp.task('watch', function () {
    // Watch the .scss files
    gulp.watch(src.sass, ['sass']);
    // Watch the .js files
    gulp.watch(src.js, ['scripts']);
    gulp.watch(src.landing, ['landingScripts']);
    // Watch the image files
    gulp.watch(src.images, ['images']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

/////////////////////////
// Start Myrnix tasks //
///////////////////////

// Concatenate and minify all of the JS files into one single js file

gulp.task('appDependencies', function () {
    return gulp.src([
        '../../node_modules/jquery/dist/jquery.js',
        '../../node_modules/jquery-ui/jquery-ui.js',
        '../../node_modules/moment/moment.js',
        '../../node_modules/angular/angular.js',
        '../../node_modules/angular-route/angular-route.js',
        '../../node_modules/angular-ui-router/release/angular-ui-router.js',
        '../../node_modules/angular-material-sidenav/angular-material-sidenav.js',
        '../../node_modules/angular-animate/angular-animate.js',
        '../../node_modules/angular-resource/angular-resource.js',
        '../../node_modules/angular-aria/angular-aria.js',
        '../../node_modules/angular-material/angular-material.js',
        '../../node_modules/angular-material-data-table/dist/md-data-table.js',
        '../../node_modules/Chart.js/Chart.js',
        '../../node_modules/angular-chart.js/dist/angular-chart.js',
        '../../node_modules/angular-ui-calendar/src/calendar.js',
        '../../node_modules/fullcalendar/dist/fullcalendar.js',
        '../../node_modules/fullcalendar/dist/gcal.js',
        '../../node_modules/highcharts/highcharts.js',
        '../../node_modules/highcharts/modules/exporting.js',
        '../../node_modules/highcharts-ng/dist/highcharts-ng.js',
        '../../node_modules/angular-accordion/js/ang-accordion.js',
        '../../node_modules/angular-ui-sortable/sortable.js',
        '../../node_modules/ng-file-upload/ng-file-upload.js',
        '../../node_modules/textAngular/dist/textAngular-rangy.min.js',
        '../../node_modules/textAngular/dist/textAngular-sanitize.js',
        '../../node_modules/textAngular/dist/textAngularSetup.js',
        '../../node_modules/textAngular/dist/textAngular.js',
        '../../node_modules/angular-ui-tree/dist/angular-ui-tree.js',
        '../../node_modules/angular-multi-step-form/dist/browser/angular-multi-step-form.js',
        '../../node_modules/lodash/lodash.js',
    '../../node_modules/angular-simple-logger/dist/angular-simple-logger.js',
    '../../node_modules/angular-google-maps/dist/angular-google-maps.js',
    '../../node_modules/ng-currency/dist/ng-currency.js',
    '../node_modules/bourbon/index.js',
  '../node_modules/bourbon-neat/index.js'])

    .pipe(concat('appDependencies.js'))
      .pipe(rename({ suffix: '.min' }))
      //.pipe(uglify())
    .pipe(gulp.dest('./build/js'));
});

gulp.task('scripts', ['ng_annotate'], function () {
    return gulp.src([
        './Scripts/app/app.js',
        './Scripts/app/controllers.js',
        './Scripts/app/services.js',
        './Scripts/app/directives.js'
    ])
    .pipe(concat('main.js'))
      .pipe(rename({ suffix: '.min' }))
      //.pipe(uglify())g
    .pipe(gulp.dest('./build/js'));
});

gulp.task('landingScripts', ['ng_annotate'], function () {
    return gulp.src([
        '../../node_modules/jquery/dist/jquery.js',
        './Scripts/landing/app.js',
        './Scripts/landing/controllers.js',
         './Scripts/landing/services.js',
        './Scripts/landing/directives.js',
        '/Scripts/landing/functions.js',
        '../../node_modules/material-design-lite/material.js',
        '../../node_modules/owl-carousel.2.0.0-beta.2.4/owl.carousel.js'

    ])
    .pipe(concat('landingMain.js'))
      .pipe(rename({ suffix: '.min' }))
      .pipe(uglify())
    .pipe(gulp.dest('./build/js'));
});

gulp.task('images', function () {
    return gulp.src('./Content/img/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
    .pipe(gulp.dest('./build/img'));
})

gulp.task('ng_annotate', function (done) {
    gulp.src('./Scripts/app/*.js')
    .pipe(ngAnnotate({ single_quotes: true }))
    .pipe(gulp.dest('./Scripts/app'))
    .on('end', done);
})
